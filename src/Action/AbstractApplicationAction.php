<?php
/**
 * contentinum-crazy-cat-components
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 11:24
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (https://www.jochum-mediaservices.de)
 */

namespace ContentinumComponents\Action;

/**
 * Class AbstractApplicationAction
 * @package ContentinumComponents\Action
 */
class AbstractApplicationAction extends AbstractContentinumAction
{
    /**
     * Zend\Expressive\Router\RouterInterface
     * @var \Zend\Expressive\Router\RouterInterface
     */
    protected $router;

    /**
     * Zend\Expressive\Template\TemplateRendererInterface
     * @var \Zend\Expressive\Template\TemplateRendererInterface
     */
    protected $template;

    /**
     * @return \Zend\Expressive\Router\RouterInterface
     */
    public function getRouter(): \Zend\Expressive\Router\RouterInterface
    {
        return $this->router;
    }

    /**
     * @param \Zend\Expressive\Router\RouterInterface $router
     * @return AbstractApplicationAction
     */
    public function setRouter(\Zend\Expressive\Router\RouterInterface $router): AbstractApplicationAction
    {
        $this->router = $router;
        return $this;
    }

    /**
     * @return \Zend\Expressive\Template\TemplateRendererInterface
     */
    public function getTemplate(): \Zend\Expressive\Template\TemplateRendererInterface
    {
        return $this->template;
    }

    /**
     * @param \Zend\Expressive\Template\TemplateRendererInterface $template
     * @return AbstractApplicationAction
     */
    public function setTemplate(\Zend\Expressive\Template\TemplateRendererInterface $template): AbstractApplicationAction
    {
        $this->template = $template;
        return $this;
    }

}