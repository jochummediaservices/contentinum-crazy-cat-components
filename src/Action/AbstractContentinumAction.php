<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 21.10.2017
 * Time: 13:12
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (https://www.jochum-mediaservices.de)
 */

namespace ContentinumComponents\Action;

/**
 * Class AbstractContentinumAction
 * @package ContentinumComponents\Action
 */
abstract class AbstractContentinumAction
{
    /**
     * Worker
     * @var \ContentinumComponents\Mapper\Worker $worker
     */
    protected $worker;

    /**
     * AbstractEntity
     * @var \ContentinumComponents\Entity\AbstractEntity $entity
     */
    protected $entity;

    /**
     * @return \ContentinumComponents\Mapper\Worker
     */
    public function getWorker(): \ContentinumComponents\Mapper\Worker
    {
        return $this->worker;
    }

    /**
     * @param \ContentinumComponents\Mapper\Worker $worker
     */
    public function setWorker(\ContentinumComponents\Mapper\Worker $worker)
    {
        $this->worker = $worker;
    }

    /**
     * @return \ContentinumComponents\Entity\AbstractEntity
     */
    public function getEntity(): \ContentinumComponents\Entity\AbstractEntity
    {
        return $this->entity;
    }

    /**
     * @param \ContentinumComponents\Entity\AbstractEntity $entity
     */
    public function setEntity(\ContentinumComponents\Entity\AbstractEntity $entity)
    {
        $this->entity = $entity;
    }

}