<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 21.10.2017
 * Time: 14:06
 */

namespace ContentinumComponents\Entity;

use ContentinumComponents\Entity\Exception\MethodNotExistsException;

abstract class AbstractEntity
{
    /**
     * Get the entity name
     * overrides method for get_class
     */
    abstract public function getEntityName();

    /**
     * Get all properties from current class as an array
     * overrides method for get_object_vars
     */
    abstract public function getProperties ();

    /**
     * Get the primary property
     * Is the same as the primary value
     */
    abstract public function getPrimaryValue ();

    /**
     * Get the primary table key name
     */
    abstract public function getPrimaryKey ();

    /**
     * Set options value if correct method available
     *
     * @param array $options
     * @return $this
     */
    public function setOptions (array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $parts = explode('_', $key);
            $method = 'set';
            foreach ($parts as $part) {
                $method .= ucfirst($part);
            }
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    /**
     * Return an associative array from object or entity properties
     *
     * @return array
     */
    public function toArray ()
    {
        $array = array();
        $properties = $this->getProperties();
        foreach ($properties as $key => $value) {
            if ('_' == substr($key,0, 1)) {
                $key = substr_replace($key, '', 0, 1);
            }
            switch ($key) {
                case 'register_date':
                case 'up_date':
                    if ($value instanceof \DateTime) {
                        $array[$key] = $value->format('YYYY-MM-dd HH:mm:ss');
                    } else {
                        $array[$key] = $value;
                    }
                    break;
                default:
                    $array[$key] = $value;
            }
        }
        return $array;
    }

    /**
     * Interzeptor method __set
     *
     * @param string $name method name
     * @param mixed $value values to set
     * @throws MethodNotExistsException
     */
    public function __set ($name, $value)
    {
        $parts = explode('_', $name);
        $method = 'set';
        foreach ($parts as $part) {
            $method .= ucfirst($part);
        }
        if (! method_exists($this, $method)) {
            throw new MethodNotExistsException('Invalid ' . get_class($this) . ' property');
        }
        $this->$method($value);
    }

    /**
     * Interzeptor method __get
     *
     * @param string $name method name
     * @throws MethodNotExistsException
     */
    public function __get ($name)
    {
        $parts = explode('_', $name);
        $method = 'get';
        foreach ($parts as $part) {
            $method .= ucfirst($part);
        }
        if (! method_exists($this, $method)) {
            throw new MethodNotExistsException('Invalid ' . get_class($this) . ' property');
        }
        return $this->$method();
    }
}