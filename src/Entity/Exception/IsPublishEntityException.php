<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 21.10.2017
 * Time: 14:03
 */

namespace ContentinumComponents\Entity\Exception;

/**
 * Class IsPublishEntityException
 * @package ContentinumComponents\Entity\Exception
 */
class IsPublishEntityException extends \InvalidArgumentException
{
}