<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 21.10.2017
 * Time: 14:04
 */

namespace ContentinumComponents\Entity\Exception;

/**
 * Class MethodNotExistsException
 * @package ContentinumComponents\Entity\Exception
 */
class MethodNotExistsException extends \BadMethodCallException
{
}