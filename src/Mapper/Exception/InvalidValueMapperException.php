<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 21.10.2017
 * Time: 13:15
 */

namespace ContentinumComponents\Mapper\Exception;

/**
 * Class InvalidValueMapperException
 * @package ContentinumComponents\Mapper\Exception
 */
class InvalidValueMapperException extends \InvalidArgumentException
{
}