<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 21.10.2017
 * Time: 13:18
 */

namespace ContentinumComponents\Mapper\Exception;

/**
 * Class MissEntityMapperException
 * @package ContentinumComponents\Mapper\Exception
 */
class MissEntityMapperException extends \RuntimeException
{
}