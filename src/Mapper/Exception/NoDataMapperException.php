<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 21.10.2017
 * Time: 13:31
 */

namespace ContentinumComponents\Mapper\Exception;

/**
 * Class NoDataMapperException
 * @package ContentinumComponents\Mapper\Exception
 */
class NoDataMapperException extends \InvalidArgumentException
{
}