<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 21.10.2017
 * Time: 13:33
 */

namespace ContentinumComponents\Mapper\Exception;

/**
 * Class SaveMapperException
 * @package ContentinumComponents\Mapper\Exception
 */
class SaveMapperException extends \InvalidArgumentException
{
}