<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 21.10.2017
 * Time: 14:13
 */

namespace ContentinumComponents\Mapper;


use Doctrine\ORM\EntityManager;

class Worker extends AbstractMapper
{
    const SAVE_INSERT = 'insert';

    const SAVE_UPDATE = 'update';

    /**
     * Worker constructor.
     * @param EntityManager $storage
     * @param string $charset
     */
    public function __construct(EntityManager $storage, $charset = 'UTF8')
    {
        $this->setStorage($storage,$charset);
    }
}