<?php
/**
 * contentinum-crazy-cat-components
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 15:44
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (https://www.jochum-mediaservices.de)
 */

namespace ContentinumComponents\Middleware\I18n;


use ContentinumComponents\Options\PageParameters;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Helper\UrlHelper;

/**
 * Class SetLocaleMiddleware
 * @package ContentinumComponents\Middleware\I18n
 */
class SetLocaleMiddleware implements MiddlewareInterface
{
    /**
     * @var UrlHelper
     */
    private $helper;

    /**
     * @var array
     */
    private $locales = array();

    /**
     * SetLocaleMiddleware constructor.
     * @param UrlHelper $helper
     * @param $locales
     */
    public function __construct(UrlHelper $helper, $locales)
    {
        $this->helper = $helper;
        $this->locales = $locales;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $uri = $request->getUri();
        $path = $uri->getPath();

        $pageOptions = PageParameters::getInstance();
        $pageOptions->setHost($uri->getHost());
        $pageOptions->setProtocol($uri->getScheme());
        $pageOptions->setPort($uri->getPort());

        if (! preg_match('#^/(?P<locale>[a-z]{2,3}([-_][a-zA-Z]{2}|))/#', $path, $matches)) {
            \Locale::setDefault($this->locales['de']);
            $pageOptions->setLocale($this->locales['de']);
            $pageOptions->setQuery($path);
            $pageOptions->generateParams();
            return $delegate->process($request);
        }

        $locale = $matches['locale'];
        $default = (isset($this->locales[$locale])) ? $this->locales[$locale] : $this->locales['de'];
        \Locale::setDefault($default);
        $pageOptions->setLocale($default);
        $path = str_replace($matches[0] , '', $path);
        $pageOptions->setQuery($path);
        $pageOptions->generateParams();
        $this->helper->setBasePath($locale);

        return $delegate->process($request->withUri($uri->withPath(substr($path,3))));

    }
}