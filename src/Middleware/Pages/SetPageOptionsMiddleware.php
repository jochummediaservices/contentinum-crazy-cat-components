<?php
/**
 * contentinum-crazy-cat-components
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 15:37
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (https://www.jochum-mediaservices.de)
 */

namespace ContentinumComponents\Middleware\Pages;


use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Helper\UrlHelper;

/**
 * Class SetPageOptionsMiddleware
 * @package ContentinumComponents\Middleware\Pages
 */
class SetPageOptionsMiddleware implements MiddlewareInterface
{
    /**
     * @var UrlHelper
     */
    private $helper;


    /**
     * SetPageOptionsMiddleware constructor.
     * @param UrlHelper $helper
     */
    public function __construct(UrlHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        return $delegate->process($request);
    }
}