<?php
/**
 * contentinum-crazy-cat-components
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 14:32
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (https://www.jochum-mediaservices.de)
 */

namespace ContentinumComponents\Options\Exception;

/**
 * Class InvalidArgumentException
 * @package ContentinumComponents\Options\Exception
 */
class InvalidArgumentException extends \InvalidArgumentException
{
}