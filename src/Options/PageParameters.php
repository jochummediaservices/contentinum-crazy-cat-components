<?php
/**
 * contentinum-crazy-cat-components
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 14:33
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (https://www.jochum-mediaservices.de)
 */

namespace ContentinumComponents\Options;


use ContentinumComponents\Options\Exception\InvalidArgumentException;
use ContentinumComponents\Tools\ArrayMergeRecursiveDistinct;

class PageParameters
{
    const URL_SEPERATOR = '/';

    const URL_I = 1;

    /**
     * instance
     *
     * Statische Variable, um die aktuelle (einzige!) Instanz dieser Klasse zu halten
     *
     * @var PageParameters
     */
    protected static $instance = null;

    /**
     *
     * @var array
     */
    protected $appProperties = [
        'controller',
        'worker',
        'entity',
        'entitymanager',
        'formfactory',
        'formdecorators',
        'form',
        'formaction',
        'formattributes',
        'formbuttons',
        'targetentities',
        'hasEntries',
        'setcategrory',
        'setcategroryvalue',
        'settoroute',
        'setexclude',
        'parentGroup',
        'populateFromRoute',
        'populateFromGroup',
        'populateFromFactory',
        'populateFromDb',
        'populateSerializeFields',
        'populateSerialize',
        'populateentity',
        'populate',
        'notpopulate',
        'services',
        'appparameter',
        'querykey'
    ];

    /**
     *
     * @var array
     */
    protected $routeParams = [
        'pages',
        'section',
        'article',
        'category',
        'categoryvalue'
    ];

    /**
     * Container standard options
     *
     * @var string
     */
    protected $standardParameters = '_default';

    /**
     * Application standard domain
     *
     * @var string
     */
    protected $standardDomain;

    /**
     *
     * @var \Zend\Permissions\Acl\Acl
     */
    protected $acl;

    /**
     *
     * @var string
     */
    protected $role;

    /**
     * Page, Default page and preferences identifier
     *
     * @var integer
     */
    protected $id;

    /**
     * Host name
     *
     * @var string
     */
    protected $host;

    /**
     * Host identifier md5 string
     *
     * @var string
     */
    protected $hostId;

    /**
     * Charset
     *
     * @var string
     */
    protected $charset;

    /**
     * Protocol
     *
     * @var string
     */
    protected $protocol;

    /**
     * Port
     *
     * @var int
     */
    protected $port;

    /**
     * REQUEST_URI
     *
     * @var string
     */
    protected $query;

    /**
     * Application locale
     *
     * @var string
     */
    protected $locale;

    /**
     *
     * @var string
     */
    protected $url;

    /**
     * Query parameter and values
     *
     * @var array
     */
    protected $params;

    /**
     * Active page url
     *
     * @var string
     */
    protected $active;

    /**
     *
     * @var string
     */
    protected $nocache;

    /**
     *
     * @var \DateTime
     */
    protected $publishUp;

    /**
     *
     * @var \DateTime
     */
    protected $publishDown;

    /**
     * Split query before further processing
     *
     * @var int
     */
    protected $splitQuery;

    /**
     * Application parameters
     *
     * @var array
     */
    protected $app = [];

    /**
     * Application asset files
     *
     * @var array
     */
    protected $assets;

    /**
     * Web page title
     *
     * @var string
     */
    protected $title;

    /**
     * Html attribute title
     * @var string
     */
    protected $linkTitle;

    /**
     * Access resource
     *
     * @var string
     */
    protected $resource;

    /**
     * Path to layout file
     *
     * @var string
     */
    protected $layout;

    /**
     * Path to template file
     *
     * @var string
     */
    protected $template;

    /**
     *
     * @var string
     */
    protected $htmlstructure;

    /**
     * Enable/Disable toolbar
     *
     * @var int
     */
    protected $toolbar = 0;

    /**
     * Enable/Disable toolbar in table row
     *
     * @var int
     */
    protected $tableedit = 0;

    /**
     *
     * @var array
     */
    protected $pageHeaders;

    /**
     * Body dom indent
     *
     * @var string
     */
    protected $bodyId;

    /**
     *
     * @var string
     */
    protected $label;

    /**
     * Page Title
     *
     * @var string
     */
    protected $metaTitle;

    /**
     * Page meta tag description
     * @var string
     */
    protected $metaDescription;

    /**
     * Page meta tag keywords
     * @var string
     */
    protected $metaKeywords;

    /**
     * User viewport
     *
     * @var string
     */
    protected $metaViewport;

    /**
     * Page meta tag robots
     * @var string
     */
    protected $robots;

    /**
     * Page content language
     * @var string
     */
    protected $language;

    /**
     * Parent page language
     * @var string
     */
    protected $languageParent;

    /**
     * Default time zone
     *
     * @var string
     */
    protected $timeZone;

    /**
     * Page meta tag author
     *
     * @var string
     */
    protected $author;

    /**
     * Google account indent
     *
     * @var string
     */
    protected $googleaccount;

    /**
     * Inline scripts in footer
     * @var string
     */
    protected $bodyFooterScript;

    /**
     * Inline scripts
     * @var string
     */
    protected $inlineScript;

    /**
     * Inline styles
     * @var string
     */
    protected $headStyle;

    /**
     * Page content
     *
     * @var array
     */
    protected $pageContent;

    /**
     * @return string
     */
    public function getStandardParameters()
    {
        return $this->standardParameters;
    }

    /**
     *
     * @param string $standardParameters
     */
    public function setStandardParameters($standardParameters)
    {
        $this->standardParameters = $standardParameters;
    }

    /**
     * @return string
     */
    public function getStandardDomain()
    {
        return $this->standardDomain;
    }

    /**
     * @param string $standardDomain
     */
    public function setStandardDomain($standardDomain)
    {
        $this->standardDomain = $standardDomain;
    }

    /**
     * @return \Zend\Permissions\Acl\Acl
     */
    public function getAcl()
    {
        return $this->acl;
    }

    /**
     * @param $acl
     */
    public function setAcl($acl)
    {
        $this->acl = $acl;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     *
     * @return string
     */
    public function getHostId()
    {
        return $this->hostId;
    }

    /**
     *
     * @param string $hostId
     */
    public function setHostId($hostId)
    {
        $this->hostId = $hostId;
    }

    /**
     *
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     *
     * @param string $charset
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

    /**
     *
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     *
     * @param string $protocol
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     *
     * @param $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     *
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     *
     * @param string $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     *
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }


    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     *
     * @param array $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param string $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getNocache()
    {
        return $this->nocache;
    }

    /**
     * @param string $nocache
     */
    public function setNocache($nocache)
    {
        $this->nocache = $nocache;
    }

    /**
     * @return \DateTime
     */
    public function getPublishUp()
    {
        return $this->publishUp;
    }

    /**
     * @param $publishUp
     */
    public function setPublishUp($publishUp)
    {
        $this->publishUp = $publishUp;
    }

    /**
     * @return \DateTime
     */
    public function getPublishDown()
    {
        return $this->publishDown;
    }

    /**
     * @param $publishDown
     */
    public function setPublishDown($publishDown)
    {
        $this->publishDown = $publishDown;
    }

    /**
     * @return int
     */
    public function getSplitQuery()
    {
        return $this->splitQuery;
    }

    /**
     * @param $splitQuery
     */
    public function setSplitQuery($splitQuery)
    {
        $this->splitQuery = $splitQuery;
    }

    /**
     * @param string|null $property
     * @return array|bool|mixed
     */
    public function getApp(string $property = null)
    {
        if (null === $property) {
            return $this->app;
        }

        if (isset($this->app[$property])) {
            return $this->app[$property];
        } else {
            return false;
        }
    }

    /**
     *
     * @param array $app
     */
    public function setApp($app)
    {
        foreach ($app as $property => $value) {
            if (in_array($property, $this->appProperties)) {
                $this->app[$property] = $value;
            }
        }
    }

    /**
     *
     * @param string $property
     * @param mixed $value
     */
    public function addAppOptions($property, $value)
    {
        if (in_array($property, $this->appProperties)) {
            $this->app[$property] = $value;
        }
    }

    /**
     * @return array
     */
    public function getAssets()
    {
        return $this->assets;
    }

    /**
     * @param array $assets
     */
    public function setAssets($assets)
    {
        if (null === $this->assets){
            $this->assets = $assets;
        } elseif ( is_string($assets) ){
            $this->assets = array('template' => '/' . $assets);
        } else {
            $this->assets = ArrayMergeRecursiveDistinct::merge($this->assets, $assets);
        }
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getLinkTitle()
    {
        return $this->linkTitle;
    }

    /**
     * @param string $linkTitle
     */
    public function setLinkTitle($linkTitle)
    {
        $this->linkTitle = $linkTitle;
    }

    /**
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @param string $resource
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
    }

    /**
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param string $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param string $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return string
     */
    public function getHtmlstructure()
    {
        return $this->htmlstructure;
    }

    /**
     * @param string $htmlstructure
     */
    public function setHtmlstructure($htmlstructure)
    {
        $this->htmlstructure = $htmlstructure;
    }

    /**
     * @return int
     */
    public function getToolbar()
    {
        return $this->toolbar;
    }

    /**
     * @param number $toolbar
     */
    public function setToolbar($toolbar)
    {
        $this->toolbar = $toolbar;
    }

    /**
     * @return int
     */
    public function getTableedit()
    {
        return $this->tableedit;
    }

    /**
     * @param number $tableedit
     */
    public function setTableedit($tableedit)
    {
        $this->tableedit = $tableedit;
    }

    /**
     * @return array
     */
    public function getPageHeaders()
    {
        return $this->pageHeaders;
    }

    /**
     * @param $pageHeaders
     */
    public function setPageHeaders($pageHeaders)
    {
        $this->pageHeaders = $pageHeaders;
    }

    /**
     * @return string
     */
    public function getBodyId()
    {
        return $this->bodyId;
    }

    /**
     * @param string $bodyId
     */
    public function setBodyId($bodyId)
    {
        $this->bodyId = $bodyId;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @param string
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $metaKeywords
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * @return string
     */
    public function getMetaViewport()
    {
        return $this->metaViewport;
    }

    /**
     * @param string $metaViewport
     */
    public function setMetaViewport($metaViewport)
    {
        $this->metaViewport = $metaViewport;
    }

    /**
     * @return string
     */
    public function getRobots()
    {
        return $this->robots;
    }

    /**
     * @param string $robots
     */
    public function setRobots($robots)
    {
        $this->robots = $robots;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getLanguageParent()
    {
        return $this->languageParent;
    }

    /**
     * @param string $languageParent
     */
    public function setLanguageParent($languageParent)
    {
        $this->languageParent = $languageParent;
    }

    /**
     * @return string
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * @param string $timeZone
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getGoogleaccount()
    {
        return $this->googleaccount;
    }

    /**
     * @param string $googleaccount
     */
    public function setGoogleaccount($googleaccount)
    {
        $this->googleaccount = $googleaccount;
    }

    /**
     * @return string
     */
    public function getBodyFooterScript()
    {
        return $this->bodyFooterScript;
    }

    /**
     * @param array $bodyFooterScript
     */
    public function setBodyFooterScript($bodyFooterScript)
    {
        $this->bodyFooterScript = $bodyFooterScript;
    }

    /**
     * @return string
     */
    public function getInlineScript()
    {
        return $this->inlineScript;
    }

    /**
     * @param string
     */
    public function setInlineScript($inlineScript)
    {
        $this->inlineScript = $inlineScript;
    }

    /**
     * @return string
     */
    public function getHeadStyle()
    {
        return $this->headStyle;
    }

    /**
     * @param string $headStyle
     */
    public function setHeadStyle($headStyle)
    {
        $this->headStyle = $this->minifyCSS($headStyle);
    }

    /**
     * @return array
     */
    public function getPageContent()
    {
        return $this->pageContent;
    }

    /**
     * @param $pageContent
     */
    public function setPageContent($pageContent)
    {
        $this->pageContent = $pageContent;
    }

    /**
     * get instance
     *
     * Falls die einzige Instanz noch nicht existiert, erstelle sie
     * Gebe die einzige Instanz dann zurück
     *
     * @return PageParameters
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     *
     * @param string $property
     * @param mixed $parameter
     */
    public function addParameter($property, $parameter)
    {
        $this->params[$property] = $parameter;
    }

    /**
     *
     * @param string $property
     * @return mixed|boolean
     */
    public function getParameter($property)
    {
        if (isset($this->params[$property]) && strlen($this->params[$property]) > 0) {
            return $this->params[$property];
        } else {
            return false;
        }
    }

    /**
     * Split page url
     *
     * @param string|null $query
     * @param int|null $i
     * @param string|null $separator
     * @param bool $remove
     * @return string
     */
    public function split(string $query = null, int $i = null, string $separator = null, bool $remove = true)
    {
        if (! $query) {
            $query = $this->query;
        }

        if (null === $i) {
            $i = self::URL_I;
        }

        if (null === $separator) {
            $separator = self::URL_SEPERATOR;
        }

        if (true === $remove) {
            if (substr($query, 0, 1) === $separator) {
                $query = substr($query, 1);
            }
        }

        return implode($separator, array_slice(explode($separator, $query), 0, $i, true));
    }

    /**
     *
     * @param string $query
     * @param string $separator
     */
    public function generateParams($query = null, $separator = null)
    {
        if (! $query) {
            $query = $this->query;
        }

        if (null === $separator) {
            $separator = self::URL_SEPERATOR;
        }
        $parts = [];
        if (null != $query) {
            $query = ltrim($query, $separator);
            $parts = explode($separator, $query);
        }
        foreach ($this->routeParams as $i => $param) {
            if (isset($parts[$i])) {
                $this->addParameter($param, $parts[$i]);
            } else {
                $this->addParameter($param, false);
            }
        }
    }

    /**
     *
     * @param array $options
     * @param string $standardParameters
     * @param bool $force
     */
    public function addOptions($options, $standardParameters = null, $force = false)
    {
        if (null !== $standardParameters) {
            $this->standardParameters = $standardParameters;
        }

        if (isset($options[$this->standardParameters])) {
            $this->setOptions($options[$this->standardParameters]);
        } else {
            if (true === $force) {
                throw new InvalidArgumentException('Parameter not found');
            }
        }
    }

    /**
     * Page attribute in Zend\Config\Config
     * @param \Zend\Config\Config $page \Zend\Config\Config
     */
    public function addPage($page)
    {
        $this->setOptions($page);
    }

    /**
     * Set page options overwrite exist parameters
     *
     * @param array|\Zend\Config\Config $options
     */
    public function setOptions($options)
    {
        $properties = get_object_vars($this);
        if (is_object($options) && method_exists($this, 'toArray')) {
            $options = $options->toArray();
        }
        foreach ($options as $property => $option) {
            if (in_array($property, $properties)) {
                switch ($property){
                    case 'host':
                    case 'scope':
                    case 'createdBy' :
                    case 'updateBy' :
                    case 'registerDate':
                    case 'upDate':
                        continue;
                        break;
                    default:
                        if (is_array($option) || strlen($option) > 0) {
                            if ('app' == $property) {
                                $this->setApp($option);
                            } elseif ('headStyle' == $property){
                                $this->setHeadStyle($option);
                            } elseif ('assets' == $property){
                                $this->setAssets($option);
                            } elseif ('id' == $property){
                                $this->setId($option);
                            } elseif ('parentPage' == $property){
                                $this->addParameter('parentPage', $option);
                            } else {
                                $this->{$property} = $option;
                            }
                        }
                } //switch
            }
        }
    }

    /**
     * Cast to array
     *
     * @return array
     */
    public function toArray()
    {
        $array = [];
        $transform = function ($letters) {
            $letter = array_shift($letters);
            return '_' . strtolower($letter);
        };
        foreach ($this as $key => $value) {
            if ($key === '__strictMode__') {
                continue;
            }
            $normalizedKey = preg_replace_callback('/([A-Z])/', $transform, $key);
            $array[$normalizedKey] = $value;
        }
        return $array;
    }

    /**
     * @param $styles
     * @return mixed|string
     */
    private function minifyCSS($styles)
    {
        $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $styles);
        $buffer = str_replace(["\r\n","\r","\n","\t",'  ','    ','     '], '', $buffer);
        $buffer = preg_replace(['(( )+{)','({( )+)'], '{', $buffer);
        $buffer = preg_replace(['(( )+})','(}( )+)','(;( )*})'], '}', $buffer);
        $buffer = preg_replace(['(;( )+)','(( )+;)'], ';', $buffer);

        return $buffer;
    }

    /**
     * clone
     *
     * Kopieren der Instanz von aussen ebenfalls verbieten
     */
    protected function __clone()
    {}

    /**
     * constructor
     *
     * externe Instanzierung verbieten
     */
    protected function __construct()
    {}

}