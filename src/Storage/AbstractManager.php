<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 21.10.2017
 * Time: 13:36
 */

namespace ContentinumComponents\Storage;


abstract class AbstractManager
{
    /**
     * Storage manager object
     * @var object
     */
    protected $storage;

    /**
     * Entity class name
     * @var string
     */
    protected $entityName;

    /**
     * Entity object
     * @var object
     */
    protected $entity;

    /**
     * Abstract function to get a storage object
     *
     * @param $storage
     * @param string $charset
     * @return mixed
     */
    abstract public function getStorage($storage = null,$charset = 'UTF8');

    /**
     * Abstract function to set a storage object
     *
     * @param object $storage
     * @param string $charset
     * @return mixed
     */
    abstract public function setStorage($storage = null, $charset = 'UTF8');

    /**
     * Abstract function to return $entityName
     * @return string
     */
    abstract public function getEntityName();

    /**
     * @param string $name
     * @return mixed
     */
    abstract public function setEntityName($name = null);

    /**
     * Abtstract function to return $_entity
     */
    abstract public function getEntity();

    /**
     * Abtstract function to set entity name
     *
     * @param object $entity
     */
    abstract public function setEntity($entity);

}