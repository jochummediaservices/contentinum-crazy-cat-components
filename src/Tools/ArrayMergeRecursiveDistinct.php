<?php
/**
 * contentinum-crazy-cat-components
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 15:13
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (https://www.jochum-mediaservices.de)
 */

namespace ContentinumComponents\Tools;

/**
 * Class ArrayMergeRecursiveDistinct
 * This array merge does not change the datatypes of the values in the arrays.
 * Matching keys' values in the second array overwrite those in the first array, as is the
 * case with array_merge, i.e.:
 *
 * self::merge(array('key' => 'org value'), array('key' => 'new value'));
 *     => array('key' => array('new value'));
 *
 * @author Daniel <daniel (at) danielsmedegaardbuus (dot) dk>
 * @author Gabriel Sobrinho <gabriel (dot) sobrinho (at) gmail (dot) com>
 * @package ContentinumComponents\Tools
 */
class ArrayMergeRecursiveDistinct
{
    /**
     * @param array $array1
     * @param array $array2
     * @return array
     */
    public static function merge(array &$array1, array &$array2)
    {
        $merged = $array1;

        foreach ($array2 as $key => &$value) {
            if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
                $merged[$key] = self::merge($merged[$key], $value);
            } else {
                $merged[$key] = $value;
            }
        }

        return $merged;
    }
}