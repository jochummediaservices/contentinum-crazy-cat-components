<?php
/**
 * contentinum-crazy-cat-components
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 15:30
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (https://www.jochum-mediaservices.de)
 */

namespace ContentinumComponents\Tools;

/**
 * Class TimeDifferenz
 * @package ContentinumComponents\Tools
 */
class TimeDifference
{
    /**
     * Timestamp
     * @var int
     */
    protected $_first = 0;
    /**
     * Timestamp
     * @var int
     */
    protected $_second = 0;
    /**
     * Construct
     * @param int $first timestamp
     * @param int $second timestamp
     */
    public function __construct ($first, $second)
    {
        $this->_first = $first;
        $this->_second = $second;
    }
    /**
     * Calculate and set labels
     */
    public function render()
    {
        return $this->makeDifferenz($this->_first,$this->_second);
    }

    /**
     * Calculate and set results in array for day, hour, min and sec
     * @param $first
     * @param $second
     * @return array
     */
    public function makeDifferenz ($first, $second)
    {
        $td = array();
        if ($first > $second) {
            $td['dif'][0] = $first - $second;
        } else {
            $td['dif'][0] = $second - $first;
        }
        $td['sec'][0] = $td['dif'][0] % 60; // 67 = 7
        $td['min'][0] = (($td['dif'][0] - $td['sec'][0]) / 60) % 60;
        $td['std'][0] = (((($td['dif'][0] - $td['sec'][0]) / 60) - $td['min'][0]) / 60) % 24;
        $td['day'][0] = floor(((((($td['dif'][0] - $td['sec'][0]) / 60) - $td['min'][0]) / 60) / 24));
        $td = $this->setLabels($td);
        return $td;
    }

    /**
     * Set labels
     * @param array $td
     * @return mixed
     */
    public function setLabels ($td)
    {
        if ($td['sec'][0] == 1) {
            $td['sec'][1] = 'Sekunde';
        } else {
            $td['sec'][1] = 'Sekunden';
        }
        if ($td['min'][0] == 1){
            $td['min'][1] = 'Minute';
        } else {
            $td['min'][1] = 'Minuten';
        }
        if ($td['std'][0] == 1) {
            $td['std'][1] = 'Stunde';
        } else {
            $td['std'][1] = 'Stunden';
        }
        if ($td['day'][0] == 1) {
            $td['day'][1] = 'Tag';
        } else {
            $td['day'][1] = 'Tage';
        }
        return $td;
    }
    /**
     * Set a time differenz string
     * @param array $td
     * @param string $spacer
     * @param bool $sec
     * @return string
     */
    public function setString($td, $spacer = ' ',$sec = false)
    {
        $str = '';
        if ( isset($td['day'][0]) && $td['day'][0] > 0 ){
            $str = $str . $td['day'][0] . ' ' . $td['day'][1] . $spacer;
        }
        if ( isset($td['std'][0]) && $td['std'][0] > 0 ){
            $str = $str . $td['std'][0] . ' ' . $td['std'][1] . $spacer;
        }
        if ( isset($td['min'][0]) && $td['min'][0] > 0 ){
            $str = $str . $td['min'][0] . ' ' . $td['min'][1];
        }
        if ( isset($td['sec'][0]) && $td['sec'][0] > 0 && false !== $sec){
            $str = $str . $spacer . $td['sec'][0] . ' ' . $td['sec'][1];
        }
        return $str;
    }
}